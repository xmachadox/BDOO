<%@page import="com.clases.Propietario"%>
<%@page import="com.clases.CasaPlaya"%>
<%@page import="com.conexion.BDOO"%>
<%@page import="com.db4o.ObjectContainer"%>
<%@page import="com.db4o.ObjectSet"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>BDOO Jesus Machado</title>  
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>	
  	</head>
  	<body>
    	<div class="container">
      		<div class="header clearfix">
        		<nav class="navbar navbar-inverse navbar-fixed-top">
      				<div class="container">
        				<div class="navbar-header">
          					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            					<span class="sr-only">Toggle navigation</span>
            					<span class="icon-bar"></span>
            					<span class="icon-bar"></span>
            					<span class="icon-bar"></span>
          					</button>
          					<a class="navbar-brand" href="index.jsp">Casa de Playas</a>
        				</div>
        				<div id="navbar" class="navbar-collapse collapse">
        					<ul class="nav navbar-nav">
        						<li>
        							<a href="registrarse.jsp">
        								Registrarse
        								
        							</a>
        						</li>
        						<li>
        							<a href="#">Link</a>
        						</li>
        					</ul>
                   <%boolean estado = false;	
                    		if(session.getAttribute("session") != null ){                     			
                    			Propietario Usuario =  (Propietario) session.getAttribute("session");
                    			estado = true;
                    		%>
                    		<ul class=" navbar-right">
        	        			<li style="color: #DDD; margin-top:5px; list-style-type: none;">
        							<div style="display: inline-block; font-weight: bold; margin-top: 10px;">
        							<%= Usuario.getLogin() %>
        							</div>
        							<div style="display: inline-block; margin-top: 5px; margin-left: 10px;">
        								<form method="post" action="DestruirSession">
        									<button type="submit" class="btn btn-sm btn-warning" name="cerrarSession" value="cerrarSession">
        										Cerrar Sesión
        									</button>
        								</form>
        							</div>
        							
        						</li>
        					</ul>
        				</div><!--/.navbar-collapse -->
      				</div>
    			</nav>
        	
      	</div>	
      	<div class="clearfix"></div>
      	<br>
      	<br>
      	<div class="container">
      	<div class="row">
      		<div class="col-lg-4">
      			<h1 class="text-center">Registrar Casas de Playa</h1>
      			<form method="post" action="RegistrarCasaPlaya" >
            		<div class="form-group">
              			<input type="text" placeholder="codigo" class="form-control" name ="codigo" required>
            		</div>	
            		<div class="form-group">
              			<input type="text" placeholder="poblacion" class="form-control" name= "poblacion" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Baños" class="form-control" name= "nbanos" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Cocinas" class="form-control" name= "ncocinas" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Comedores" class="form-control" name= "ncomedores" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Estacionamientos" class="form-control" name= "nestacionamientos" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Habitaciones" class="form-control" name= "nhabitaciones" required>
            		</div>
            		<button type="submit" class="btn btn-success" name="registrarcasaplaya"  value="registrarcasaplaya">Guardar</button>
          		</form>
      			
      		</div>
      		<div class="col-lg-8">
      			<h1>Casas de Playa Registradas</h1> 
      			<table class="table table-hover">
      				<thead>
      					<th>Codigo</th>
      					<th>EstadoActual</th>
      					<th>#-Banos</th>
      					<th>#-Cocinas</th>
      					<th>#-Comedores</th>
      					<th>#-Estacionamientos</th>
      					<th>#-Habitaciones</th>
      					<th>#-Poblacion</th>
      					<th>#-Propietario</th>
      				</thead>
      				<%
      					ObjectContainer db = BDOO.obtenerInstancia(); 
      					ObjectSet<CasaPlaya> resultCasaPlaya =  db.queryByExample(new CasaPlaya(null,null,null,null,null,null,null,true,null,Usuario));
      					CasaPlaya x = null;
      				%>
      				<tbody>
      				<%while(resultCasaPlaya.hasNext()){ %>
      					x = resultCasaPlaya.next();
      					<tr>
      						<td><%= x.getCodigo() %></td>
      						<td><%= x.isEstadoactual() %></td>
      						<td><%= x.getNbanos() %></td>
      						<td><%= x.getNcocinas() %></td>
      						<td><%= x.getNcomedores() %></td>
      						<td><%= x.getNestacionamientos() %></td>
      						<td><%= x.getNhabitaciones() %></td>
      						<td><%= x.getPoblacion()  %></td>
      						<td><%= x.getPropietario().getLogin() %></td>
      					</tr>
      				<%} %>
      				</tbody>
  				</table>
      		</div>
      	
      	</div>
      	</div>
        					<%} else{ %>
        					<!--  
		 					<ul class=" navbar-right">
        	        			<li style="color: #DDD; margin-top:5px; list-style-type: none;">
        							<div style="display: inline-block; font-weight: bold; margin-top: 10px;">
        							</div>
        							<div style="display: inline-block; margin-top: 5px; margin-left: 10px;">
        								<form method="post" action="Dsession">
        									<button type="submit" class="btn btn-sm btn-warning" name="cerrarSession" value="cerrarSession">
        										Cerrar Sesión
        									</button>
        								</form>
        							</div>
        						</li>
        					</ul>   
        					-->	
        					
          					<form class="navbar-form navbar-right" method="post" action="Login" >
            					<div class="form-group">
              						<input type="text" placeholder="Login" class="form-control" name ="login">
            					</div>	
            					<div class="form-group">
              						<input type="password" placeholder="Clave" class="form-control" name= "password">
            					</div>
            					<button type="submit" class="btn btn-success" name="loginuser"  value="loginuser">Loguearse</button>
          					</form>
          				</div><!--/.navbar-collapse -->
      				</div>
    			</nav>
        	
      	</div>	
                  
    		<%} %>
    			
      

     

   

    </div> <!-- /container -->
</html>